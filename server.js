const express = require('express')();
const http = require('http').createServer(express);
const io = require('socket.io')(http);
const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://user-chat:alpine@cluster0.dflh7.mongodb.net/chat?retryWrites=true&w=majority";
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

client.connect((err, db) => {
    if (err) {
        process.exit(1);
    } else {
        // declare collection mongo
        let collection = db.db('chat').collection('messages');
        let userCountList = 0;

        express.get('/', (req, res) => {
            res.sendFile(__dirname + '/index.html');
        });

        http.listen(3000, () => {
            console.log('Server started on port : 3000');
        });

        io.on('connection', (socket) => {
            io.emit('chat message', 'Connexion d\'un utilisateur');

            collection.find({}, { projection: { _id: 0 } }).toArray(function(err, result) {
                if (err) throw err;
                for (let i = 0; i < result.length; i++) {
                    console.log(result.length);
                    console.log(result[i].message);
                    socket.emit('stocked messages', result[i].message);
                };

            });
            userCountList++;

            socket.on('disconnect', () => {
                io.emit('chat message', 'Deconnexion d\'un utilisateur');
                userCountList--;
                countUsers(userCountList);
            });
            countUsers(userCountList);

            socket.on('chat message', (msg) => {
                io.emit('chat message', msg);
                let message = { message: msg }
                collection.insertOne(message, function(err, res) {
                    if (err) throw err;
                });
            });

            function countUsers(users) {
                io.emit('userCountList', userCountList)
                console.log(`Utilisateurs en ligne : ${userCountList}`)
            };
        });
    };
});